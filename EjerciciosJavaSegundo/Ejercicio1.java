import java.util.Scanner;

/*1. Pedir una nota de 0 a 10 y mostrarla de la forma: Insuficiente,
Suficiente, Bien...*/

public class Ejercicio1 {

	
	public static void main(String[] args) {
		

		System.out.println("Introduce tu nota");	
		
		Scanner sc = new Scanner (System.in);
		
		int numero = sc.nextInt();
		
		if (numero<5){		
			System.out.println("Insuficiente");
		
		}else if (numero==5){			
			System.out.println("Suficiente");
			
		}else{
			System.out.println("Bien");
		}
		
		sc.close();
	}

}
